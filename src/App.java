import java.util.Arrays;
import java.util.List;
import java.util.ArrayList; //6
import java.util.Random;//7
import java.util.*;

public class App {

    public static void main(String[] args) throws Exception {

        // 1 Kiểm tra một biến có phải mảng hay không
        String a1 = "Devcamp";
        int[] a2 = { 1, 2, 3 };
        String a3 = "(‘Devcamp User’).split()";

        System.out.printf("Is %s an array - %s", a1, a1.getClass().isArray());
        System.out.println();

        System.out.printf("Is %s an array - %s", a2, a2.getClass().isArray());
        System.out.println();

        System.out.printf("Is %s an array - %s", a3, a3.getClass().isArray());
        System.out.println();

        // 2 Trả về phần tử thứ n của mảng
        int[] a4 = { 1, 2, 3, 4, 5, 6 };

        System.out.println("Phần tử thứ 3 của mảng a4");
        System.out.println(a4[3]);

        // System.out.println("Phần tử thứ 6 của mảng a4");
        // System.out.println(a4[6]);

        // 3 Sắp xếp giá trị các phần tử của mảng theo thứ tự tăng dần
        int[] a5 = { 3, 8, 7, 6, 5, -4, -3, 2, 1 };
        Arrays.sort(a5);
        System.out.println("\nThe sorted array is: ");
        for (int num : a5) {
            System.out.print(num + " ");
        }
        ;

        // 4 Trả về vị trí của phần tử thứ n trong mảng
        int[] my_array = { 1, 2, 3, 4, 5, 6 };
        // find the index of 3
        System.out.println("4.Index position of 3 is: " + findIndex(my_array, 3));
        // find the index of 7
        System.out.println("4.Index position of 7 is: " + findIndex(my_array, 7));

        // 5 Nối 2 mảng với nhau để tạo thành một mảng mới
        int[] array1 = { 1, 2, 3 };
        int[] array2 = { 4, 5, 6 };

        int aLen = array1.length;
        int bLen = array2.length;
        int[] result = new int[aLen + bLen];

        System.arraycopy(array1, 0, result, 0, aLen);
        System.arraycopy(array2, 0, result, aLen, bLen);

        System.out.println("5. Concernate two arrays: " + Arrays.toString(result));

        // 6 Lọc một mảng trả ra một mảng chứa các phần tử có giá trị (số hoặc chuỗi)
        // Object[] array = {Double.NaN, 0, 15, false, -22, "", "undefined", 47, null};
        List<Object> list = new ArrayList<>();
        list.add(Double.NaN);
        list.add(0);
        list.add(15);
        list.add(false);
        list.add(-22);
        list.add("");
        list.add("undifined");
        list.add(47);
        list.add(null);
        List<Object> filters = new ArrayList<>();
        filters.add(Double.NaN);
        filters.add(false);
        filters.add("");
        filters.add("undifined");
        filters.add(null);
        list.removeAll(filters);
        System.out.println("6. Filtered List " + list);

        //7 Bỏ phần tử có giá trị bằng n trong mảng cho trước
        int[] array = { 2, 5, 9, 6 };
        int key = 5;
        array = removeElements(array, key);
        System.out.println("7. Remove key = 5 from array: " + Arrays.toString(array));

        // 8 Lấy random một phần tử bất kỳ trong mảng
        int myArray[] = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        System.out.println("8. Ramdom element " + getRandomValue(myArray));
        System.out.println("8. Ramdom element " + getRandomValue(myArray));


        //9 Tạo một mảng gồm x phần tử có giá trị y
        int array9[] = new int[6];  // Empty Array Declaration
        Arrays.fill(array9, 0);  // Filling the data
        // Printing the data
        System.out.println("9. Array completely filled with 10\n"
                + Arrays.toString(array9));

    
        //10  Tạo một mảng gồm y số liên tiếp bắt đầu từ giá trị x
        int array10[] = new int[4]; // Array Declaration
         
        // Adding elements in the array
        for (int i = 0; i < array10.length; i++)
        {
            array10[i] = i + 1;
        }
         
        // Printing the elements
        for (int i = 0; i < array10.length; i++)
        {
            System.out.print("10. Array from 1: " + array10[i] + " ");
        }

    }

    // 4
    public static int findIndex(int arr[], int t) {
        int index = Arrays.binarySearch(arr, t);
        return (index < 0) ? -1 : index;
    }

    //7 
    // function to remove all occurrences
    // of an element from an array
    public static int[] removeElements(int[] arr, int key)
    {
        // Move all other elements to beginning
        int index = 0;
        for (int i=0; i<arr.length; i++)
            if (arr[i] != key)
                arr[index++] = arr[i];
 
        // Create a copy of arr[]
        return Arrays.copyOf(arr, index);
    }

    // 8
    public static int getRandomValue(int[] array) {
        int result = new Random().nextInt(array.length);
        return array[result];
    }



}
